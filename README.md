### movies_comments

## Requirements
	node => 8.11.2
	npm
## Usage
	Install
	~ npm install
	Start
	~ npm start
	Test
	~ npm test
## API Documentation
App is deployed here tycjan.herokuapp.com
I provide html-based api 
docs on main /GET[tycjan.herokuapp.com](https://tycjan.herokuapp.com/)

Here is Copy

	GET tycjan.herokuapp.com html page with api documentation

	GET tycjan.herokuapp.com/movies Get all movies in db 

	GET tycjan.herokuapp.com/movies/:_id Get one movie by id 

	POST tycjan.herokuapp.com/movies You must specify "application/json" in headers and pass json with title parameter example -> {"title":"Movie title"}

	POST tycjan.herokuapp.com/comments You must specify "application/json" in headers and pass json with movieId and content parameters example -> {"movieId":"123435545fv", "content": "Some nice comment"} 

	GET tycjan.herokuapp.com/comments Get all comments in db 
	GET tycjan.herokuapp.com/comments/:movie_id Get all comments associated 	with movie 
