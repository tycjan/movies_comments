import {App} from "./App";

const app: App = new App();
app.listen();
app.connectToDb()
    .then(() => {
        console.log("Connected to DB");
    })
    .catch((err) => {
        console.log(err);
    });