import {Column, CreateDateColumn, Entity, ObjectID, ObjectIdColumn, OneToMany} from "typeorm";
import {Comment} from './Comment';

@Entity()
export class Movie {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    title: string;

    @Column()
    year: number;

    @Column()
    release: string;

    @Column()
    genre: string;

    @Column()
    director: string;

    @Column()
    actors: string;

    @Column()
    plot: string;

    @Column()
    imdbRating: number;

    @Column()
    type: string;

    @Column()
    country: string;

    @CreateDateColumn()

    @OneToMany(type => Comment, comment => comment.movie)
    comments: Comment[];
}