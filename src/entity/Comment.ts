import {Column, CreateDateColumn, Entity, ManyToOne, ObjectID, ObjectIdColumn} from "typeorm";
import {Movie} from "./Movie";

@Entity()
export class Comment {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    movieId: string;

    @Column()
    content: string;

    @CreateDateColumn()
    created: Date;

    @ManyToOne(type => Movie, movie => movie.comments)
    movie: Movie;
}