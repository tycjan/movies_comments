export class EndpointProvider {

    public hostname: string;
    public key: string;

    constructor(hostname, key) {
        this.hostname = hostname;
        this.key = key;
    }

    public buildUrlEndpointWithParam(param): string {
        return `${this.hostname}${this.key}${param}`;
    }
}