import * as methodOverride from 'method-override';
import {createConnection} from "typeorm";
import * as express from 'express';
import * as cors from 'cors';

import {CommentController} from "./controllers/CommentController";
import {MovieController} from "./controllers/MovieController";
import {DefaultRouter} from "./routes/DefaultRouter";

export class App {

    public app: express.Application;
    public movieController: MovieController;
    public commentController: CommentController;
    public defaultRouter: DefaultRouter;

    constructor() {
        this.app = express();
        this.movieController = new MovieController();
        this.commentController = new CommentController();
        this.defaultRouter = new DefaultRouter();
        this.setRoutes();
        this.setMiddleware();
        this.setDefaultRoute();
    }

    public setMiddleware(): void {

        this.app.use(methodOverride('X-HTTP-Method-Override'));
        this.app.use(cors());
    }

    public setRoutes(): void {

        this.app.use(this.movieController.router);
        this.app.use(this.commentController.router);
    }

    public async connectToDb(): Promise<void> {
        await createConnection();
    }

    public listen(): void {
        this.app.listen(process.env.PORT);
    }

    private setDefaultRoute() {
        this.defaultRouter.routes(this.app);
    }
}