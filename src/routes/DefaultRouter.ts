import * as express from "express";
import * as path from 'path';

export class DefaultRouter {
    public pathToIndex: string;

    constructor() {
        this.pathToIndex = '/../public/index.html'
    }

    public routes(app: express.Application) {
        app.use((req, res) => {
            res.sendFile(path.join(__dirname + this.pathToIndex));
        });
    }
}