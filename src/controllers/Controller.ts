import {Router} from "express";

export abstract class Controller {

    router: Router;

    protected constructor() {
        this.router = Router();
    }
}