import {getManager, ObjectIdColumn} from "typeorm";
import {Request, Response} from "express";
import * as bodyParser from "body-parser";

import {Comment} from "../entity/Comment";
import {Controller} from "./Controller";
import {Movie} from "../entity/Movie";

export class CommentController extends Controller {

    constructor() {
        super();
        this.routes();
    }

    public routes(): void {

        const jsonParser = bodyParser.json();
        this.router.post('/comments', jsonParser, this.create);
        this.router.get('/comments', this.findAll);
        this.router.get('/comments/:_movie_id', this.findByMovieId);
    }

    public create(req: Request, res: Response): void {

        const content = req.body.content;
        const movieObjectId = ObjectIdColumn(req.body.movieId);
        const movieId = req.body.movieId;

        if (content == null || movieObjectId == null || movieId == null) {
            res.statusCode = 400;
            res.send({"Error": "content and movieId must be specified!"});
            return;
        }

        getManager().findOne(Movie, {id: movieObjectId})
            .then((movie) => {
                if (movie === undefined) {
                    throw new Error(`No such movie associated with id: ${movieId}`);
                }
                let newComment: Comment = new Comment();
                newComment.content = content;
                newComment.movieId = movieId;
                newComment.movie = movie;
                getManager().save(newComment).then((comment) => {
                    res.send(comment);
                });
            })
            .catch((err) => {
                res.statusCode = 404;
                res.send({'Error': err.toString()});
            });
    }

    public findAll(req: Request, res: Response): void {

        getManager().find(Comment)
            .then((comments) => {
                res.send(comments);
            });
    }

    public findByMovieId(req: Request, res: Response): void {

        const movieId = req.params._movie_id;
        if (movieId == null) {
            res.statusCode = 400;
            res.send({'Error': 'Movie id must specified'});
        }
        getManager().find(Comment, {movieId: movieId})
            .then((comments) => {
                if (comments === undefined) {
                    throw new Error(`No such movie associated with id: ${movieId}`);
                }
                res.send(comments);
            })
            .catch((err) => {
                res.statusCode = 404;
                res.send({'Error': err.toString()})
            });
    }
}