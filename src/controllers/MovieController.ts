import {getManager, ObjectIdColumn} from 'typeorm';
import * as bodyParser from 'body-parser';
import {Request, Response} from 'express';
import fetch from 'node-fetch';

import {Movie} from '../entity/Movie';
import {Controller} from './Controller';
import {EndpointProvider} from "../providers/EndpointProvider";

export class MovieController extends Controller {

    constructor() {
        super();
        this.routes();
    }

    public routes(): void {

        const jsonParser = bodyParser.json();
        this.router.post('/movies', jsonParser, this.create);
        this.router.get('/movies', this.findAll);
        this.router.get('/movies/:_id', this.findById);
    }

    public create(req: Request, res: Response): void {
        const title = req.body.title;
        if (title == null) {
            res.statusCode = 400;
            res.send({'Error': 'You must specify title'});
            return;
        }
        let endpointProvider: EndpointProvider = new EndpointProvider('http://www.omdbapi.com/', '?apikey=c1eb1d3',);
        const param: string = `&t=${encodeURIComponent(title)}`;
        const endpoint: string = endpointProvider.buildUrlEndpointWithParam(param);
        fetch(endpoint, {method: 'GET'})
            .then(res => res.json())
            .then((json) => {
                if (json.Response === 'False') {
                    throw new Error('Movie not found!');
                }
                let newMovie: Movie = new Movie();
                newMovie.title = json.Title;
                newMovie.actors = json.Actors;
                newMovie.genre = json.Genre;
                newMovie.plot = json.Plot;
                newMovie.director = json.Director;
                newMovie.year = json.Year;
                newMovie.country = json.Country;
                newMovie.imdbRating = json.imdbRating;
                newMovie.type = json.Type;
                getManager().save(newMovie).then((movie) => {
                    res.send(movie);
                });
            })
            .catch((err) => {
                res.statusCode = 400;
                res.send({'Error': err.toString()});
            });
    }

    public findAll(req: Request, res: Response): void {

        getManager().find(Movie).then((movies) => {
            res.send(movies);
        });
    }

    public findById(req: Request, res: Response): void {

        const movieId = ObjectIdColumn(req.params._id);
        if (movieId == null) {
            res.statusCode = 400;
            res.send({'Error': 'id must specified'});
        }

        getManager().findOne(Movie, {id: movieId})
            .then((movie) => {
                if (movie === undefined) {
                    throw new Error(`No such movie associated with requested id ${movieId}`);
                }
                res.send(movie);
            })
            .catch((err) => {
                res.statusCode = 404;
                res.send({'Error': err.toString()});
            })
    }
}