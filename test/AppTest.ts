import {suite, test,} from 'mocha-typescript';
import {mock, when, verify} from 'ts-mockito';
import {createConnection} from "typeorm";
import {App} from '../src/App';

@suite
class AppTest {
    @test connectToDbTest() {
        //given
        let appMock: App = mock(App);

        //when
        when(appMock.connectToDb()).thenCall(() =>{
            let connection = mock(createConnection);
            when(connection()).thenResolve();

            //then
            verify(createConnection).called();
        });
    }
}



