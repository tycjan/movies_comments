import {suite, test} from 'mocha-typescript';
import {MovieController} from "../src/controllers/MovieController";
import { expect } from 'chai'
import * as fetchMock from 'fetch-mock';
import {mockReq, mockRes} from 'sinon-express-mock';

@suite class MovieControllerTest {

    @test postMovie() {
        //given
        let movieController: MovieController = new MovieController();
        const movie: string = "Movie";

        const request = {
            body: {
                title: "",
            },
        };

        const req = mockReq(request);
        const res = mockRes();
        const endpoint: string = `http://www.omdbapi.com/?apikey=c1eb1d3&t=${movie}`;
        fetchMock.get(endpoint, {
            "Response": "False"
        });

        //then and when
        expect(movieController.create(req, res)).to.be.undefined;
    }
}