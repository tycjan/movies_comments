import {suite, test} from 'mocha-typescript';
import * as chai from 'chai';

import {App} from '../src/App';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);
const expect = chai.expect;

@suite
class DefaultRouterTest {

    @test defaultRouterTestWithExactPath() {
        //given
        const app: App = new App();
        const exactPath: string = '/';
        //when
        chai.request(app.app)
            .get(exactPath)
            .end((err, res) => {
                //then
                expect(res).to.have.status(200);
                expect(res).to.be.html;
            });
    }

    @test defaultRouterTestWithRandomPath() {
        //given
        const app: App = new App();
        const exactPath: string = '/some-random-path';
        //when
        chai.request(app.app)
            .get(exactPath)
            .end((err, res) => {
                //then
                expect(res).to.have.status(200);
                expect(res).to.be.html;
            });
    }
}