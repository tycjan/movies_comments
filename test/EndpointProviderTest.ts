import {suite, test} from 'mocha-typescript';
import {expect} from 'chai';
import {EndpointProvider} from '../src/providers/EndpointProvider'

@suite
class EndpointProviderTest {
    @test buildUrlWithParamTest() {
        //given
        const hostname: string = 'some-hostname';
        const key: string = '-some-key';
        const param: string = '-some-params';
        const expectedResult: string = 'some-hostname-some-key-some-params';
        const endpointProvider: EndpointProvider = new EndpointProvider(hostname, key);

        //when
        const buildUrlTestResult: string = endpointProvider.buildUrlEndpointWithParam(param);

        //then
        expect(buildUrlTestResult).to.equal(expectedResult);
    }
}


